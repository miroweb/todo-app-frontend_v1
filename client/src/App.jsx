import {
  AppBar,
  Button,
  Container,
  CssBaseline,
  Grid,
  Toolbar,
  Typography,
  TextField,
  Box,
  List,
  ListItem,
  IconButton,
  ListItemText
} from '@material-ui/core';
import { AddToQueueOutlined, Create, Delete, Edit } from '@material-ui/icons';
import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import './App.css';
import socketClient from 'socket.io-client';

const App = () => {
  const SERVER = 'http://localhost:8080';
  var socket = socketClient(SERVER);

  socket.on('todo created', function (msg) {
    console.log('Jemand hat ein todo erstellt:', msg);
  });

  const [todo, setTodo] = useState('');
  const [todos, setTodos] = useState([]);
  const [todoId, setTodoId] = useState('');
  const [isEdit, setIsEdit] = useState(false);

  const classes = useStyles();
  const addTodo = () => {
    socket.emit('todo created', todo);
    fetch('http://localhost:5000/api/todos', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify({ title: todo })
    })
      .then((res) => {
        setTodo('');
        getTodos();
      })
      .catch((error) => console.log(error));
  };

  const updateTodo = () => {
    fetch(`http://localhost:5000/api/todos/${todoId}`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'put',
      body: JSON.stringify(todo)
    })
      .then(() => {
        setTodo('');
        getTodos();
        setTodoId('');
        setIsEdit(false);
      })
      .catch((error) => console.log(error));
  };

  const deleteTodo = (id) => {
    fetch('http://localhost:5000/api/todos/' + id, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'delete',
      body: JSON.stringify({ _id: id })
    })
      .then(() => {
        getTodos();
      })
      .catch((error) => console.log(error));
  };

  const setEdit = (id, title) => {
    setIsEdit(!isEdit);
    setTodoId(id);
    setTodo(title);
  };

  const submitTodo = (e) => {
    if (e.key === 'Enter') {
      addTodo();
    }
  };

  function getTodos() {
    fetch('http://localhost:5000/api/todos', {
      method: 'GET'
    })
      .then((res) => {
        return res.json();
      })
      .then((todos) => {
        setTodos(todos.data);
      });
  }

  const onChange = (todoId, e) => {
    //console.log('checked?', todoId, e.target.checked);
    setEdit(todoId);
    updateTodo();
  };

  useEffect(() => {
    getTodos();
  }, []);

  return (
    <>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <Create className={classes.icon} />
          <Typography variant="h6">ToDo-Liste</Typography>
        </Toolbar>
      </AppBar>
      <main>
        <div className="todo-form">
          <TextField
            id="outlined-basic"
            label="Task"
            variant="outlined"
            className="todo-form--field"
            onChange={(e) => setTodo(e.target.value)}
            value={todo}
            onKeyPress={(e) => submitTodo(e)}
          />
          {isEdit ? (
            <Button
              variant="contained"
              color="primary"
              onClick={() => updateTodo()}
            >
              UPDATE
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={() => addTodo()}
            >
              ADD
            </Button>
          )}
        </div>

        <div className="list-container">
          <Grid item xs={12} md={6}>
            <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
              Meine Aufgaben
            </Typography>
            <div className="list">
              <List dense={false}>
                {todos.length > 0
                  ? todos.map((item, index) => {
                      return (
                        <ListItem key={index}>
                          <ListItemText primary={item.title} />
                          <input
                            type="checkbox"
                            checked={item.completed}
                            onChange={(e) => onChange(item._id, e)}
                          />
                          <IconButton
                            edge="end"
                            aria-label="edit"
                            onClick={() => setEdit(item._id, item.title)}
                          >
                            <Edit className={classes.icon} />
                          </IconButton>
                          <IconButton
                            edge="end"
                            aria-label="delete"
                            onClick={() => deleteTodo(item._id)}
                          >
                            <Delete
                              className={classes.icon}
                              style={{ color: 'red' }}
                            />
                          </IconButton>
                        </ListItem>
                      );
                    })
                  : null}
              </List>
            </div>
          </Grid>
        </div>
        {/* <div className={classes.container}>
          <Container maxWidth="sm">
            <Typography
              variant="h2"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              Aufgaben
            </Typography>
            <Typography
              variant="h5"
              align="center"
              color="textSecondary"
              paragraph
            >
              Willkommen auf deiner ToDo-Liste!
            </Typography>
          </Container>
        </div>

        <div className={classes.buttons}>
          <Grid container spacing={2} justify="center">
            <Grid item>
              <Button variant="contained" color="primary">
                Add
              </Button>
            </Grid>
            <Grid item>
              <Button variant="outlined" color="primary">
                Delete
              </Button>
            </Grid>
          </Grid>
        </div> */}
      </main>
    </>
  );
};

export default App;
