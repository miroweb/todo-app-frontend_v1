# Einfache todo-app mit Grundfunktionen

- Erster Entwurf von Frontend. Todo App mit React, Node.js & MongoDB
- Funktioniert mit Backend via MongoDB Atlas Database. Name der Database: mern-todo-app.


# Installation

- Clientseitig

- [x] `cd client`
- [x] `npm install`
- [x] `npm start`

- Serverseitig

- [x] `cd server`
- [x] `npm install`
- [x] `npm run dev`

## API Endpunkte

- [ ] Get all todos: /api/todos
- [ ] Create new todo: /api/todos
- [ ] Edit a todo item: /api/todos/:itemId
- [ ] Delete a todo item: /api/todos/:itemId
