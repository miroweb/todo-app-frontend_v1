import mongoose from 'mongoose';

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(
      'mongodb+srv://admin_1974:VZCsPV3Eh362Zk3G@cluster0.bvr0k.mongodb.net/mern-todo-app?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,

        useUnifiedTopology: true
      }
    );
    mongoose.connection.once('open', function () {
      console.log('connection established');
    });
    // const conn = await mongoose.connect(`${process.env.MONGO_URI}`, {
    //   useUnifiedTopology: true,
    //   useNewUrlParser: true,
    // });
    console.log(`MongoDB connected: ${conn.connection.host}`);
  } catch (error) {
    console.log(`MognoDB Error: , ${error.message}`);
    process.exit(1);
  }
};

export default connectDB;
