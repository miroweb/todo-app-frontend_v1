import express from 'express';
import connectDB from './config/db.js';
import { notFound, errorHandler } from './middleware/errorMiddleware.js';
import dotenv from 'dotenv';
import todoRoutes from './routes/todoRoutes.js';
import cors from 'cors';
import { Server } from 'socket.io';
import http from 'http';
import bodyParser from 'body-parser';

// load env vairables
dotenv.config();
connectDB();
const app = express();
app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded());

const server = http.createServer(app);
const io = new Server(server, { cors: { origin: '*' } });

server.listen(8080, () => {
  console.log('Socket.io läuft auf 8080');
});

io.on('connection', (socket) => {
  console.log('A user connected');

  socket.on('todo created', (msg) => {
    io.emit('todo created', msg);
  });
});

// Routes
app.get('/', (req, res) => {
  res.send('Server running and up...🔥');
});
app.use('/api/todos', todoRoutes);

// Middleware
app.use(notFound);
app.use(errorHandler);

// Server listening
const PORT = process.env.PORT || 5001;
app.listen(PORT, () => {
  console.log(`\nServer listening on port ${PORT}`);
});
